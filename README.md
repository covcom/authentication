
# Authentication API

This repository contains a simple API that implements **HTTP Basic Authentication**. You can use this as the basis of the authentication for your assignment.

## 1 Getting Started

You should start by cloning the repository to your development workstation. It can be started via `node index.js` and will provide an API by default on port `8080`. It provides two main routes:

| Route                   | Purpose |
| ----------------------- | ------- |
| `POST /users`           | This is used to create a new user account. The request body should contain a JSON string defining any data you want to store. |
| `HEAD /users/:username` | This is used to validate a username and password combination, substitute a username where indicated in the url. |

To illustrate its usage we will explore how to use it using the `curl` tool but you can make use of more visual tools such as [Postman](https://www.getpostman.com).

Before we can make the API call we need to prepare the authorization data to conform to the http basic auth standard. To do this we need to:

1. Combine the username and password separated with a colon, for example `johndoe:p455w0rd`.
2. Base64 encode this, for example `am9obmRvZTpwNDU1dzByZA==`
3. Create the auth string be prepending with the `Basic` string, for example `Basic am9obmRvZTpwNDU1dzByZA==`.

To base64 encode the string you can either use the terminal with the `echo -n 'johndoe:p455w0rd' | openssl base64` command or use one of an [online tool](https://www.base64encode.org). If you need to generate this using JavaScript you can use the following script:

```javascript
const hash = Buffer.from('johndoe:P455w0rd').toString('base64')
const header = `Basic ${hash}`
```

Now we have the encoded string we can pass this as the `Authorization` header thus:

```shell
$ curl -i -X POST -H "Authorization: Basic amRvZTpwNDU1dzByZA==" localhost:8080/users
  HTTP/1.1 201 Created
  Access-Control-Allow-Origin: *
  Content-Type: application/json; charset=utf-8
  Allow: POST
  Content-Length: 20
  Date: Sun, 25 Nov 2018 19:07:38 GMT
  Connection: keep-alive

  {"status":"CREATED"}
```

You would normally want to pass some data in the request body and this can be done as shown (line breaks can be inserted using the backslash):

```shell
$ curl -i -X POST -H "Authorization: Basic amFuZWRvZTpwNDU1dzByZA=" \
  -H "Content-Type: application/json" \
  -d '{"type":"user"}' localhost:8080/users

  HTTP/1.1 201 Created
  Access-Control-Allow-Origin: *
  Content-Type: application/json; charset=utf-8
  Allow: POST
  Content-Length: 20
  Date: Mon, 26 Nov 2018 07:59:03 GMT
```

## 2 Validating a User

Once we have an account set up we can use the `HEAD /users/:username` route to see if a supplied username and password are valid or not. Again, we need to generate the `Authorization` request header using the username and password we want to validate but we also need to add the username to the end of the url.

```shell
$ curl -i -X HEAD -H "Authorization: Basic amFuZWRvZTpwNDU1dzByZA=" localhost:8080/users/johndoe
  HTTP/1.1 200 OK
  Access-Control-Allow-Origin: *
  Content-Type: application/json; charset=utf-8
  Content-Length: 28
  Date: Sun, 25 Nov 2018 19:13:20 GMT
```

If we supply an incorrect username or password in our header we get a different status code:

```shell
$ curl -i -X HEAD -H "Authorization: Basic amFuZWRvZTpwYXNzd29yZA==" localhost:8080/users/johndoe
  HTTP/1.1 401 Unauthorized
  Access-Control-Allow-Origin: *
  Content-Type: application/json; charset=utf-8
  Content-Length: 52
  Date: Sun, 25 Nov 2018 19:15:03 GMT
```

In both these cases you get a warning from curl to use the `-I` flag for a `HEAD` request so the following is preferred:

```shell
$ curl -i -I -H "Authorization: Basic amFuZWRvZTpwYXNzd29yZA==" localhost:8080/users/johndoe
  HTTP/1.1 401 Unauthorized
  Access-Control-Allow-Origin: *
  Content-Type: application/json; charset=utf-8
  Content-Length: 52
  Date: Sun, 25 Nov 2018 19:16:57 GMT
```

## 3 Applying This Knowledge

What this means for your app is that you should include the `Authorization` header in every request the client (web page/app) makes to the API. In real terms this means that your "login" page should:

1. Take the username and password entered by the user
2. Use this to generate the `Authorization` header
3. Make an API `HEAD` call:
    1. If the status code is `200 OK` you can cache these credentials and include them with every request. You can store this using http local storage or equivalent.
    2. If the status code is `401 UNAUTHORIZED` you need to display a message to the user and ask them to try again.
    3. When the use clicks the log out button you simply delete the locally cached credentials.
