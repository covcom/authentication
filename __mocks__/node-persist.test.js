
'use strict'

const persist = require('./node-persist.x.js')

describe('init', () => {

})

describe('setItem', () => {

	test('save a single item', async done => {
		await persist.init()
		await persist.setItem('johndoe', {role: 'user'})
		const data = await persist.getItem('johndoe')
		expect(data).toEqual({role: 'user'})
		done()
	})

	test('save multiple items', async done => {
		await persist.init()
		await persist.setItem('johndoe', {role: 'user'})
		await persist.setItem('janedoe', {role: 'admin'})
		const john = await persist.getItem('johndoe')
		expect(john).toEqual({role: 'user'})
		const jane = await persist.getItem('janedoe')
		expect(jane).toEqual({role: 'admin'})
		done()
	})

})

describe('getItem', () => {

	test('retrieve a single item', async done => {
		await persist.init()
		await persist.setItem('johndoe', {role: 'user'})
		const data = await persist.getItem('johndoe')
		expect(data).toEqual({role: 'user'})
		done()
	})

	test('retrieving non-existent item returns undefined', async done => {
		await persist.init()
		await persist.setItem('johndoe', {role: 'user'})
		const data = await persist.getItem('janedoe')
		expect(data).toEqual(undefined)
		done()
	})

})

describe('removeItem', () => {

	test('remove a single item', async done => {
		await persist.init()
		await persist.setItem('johndoe', {role: 'user'})
		await persist.setItem('janedoe', {role: 'admin'})
		await persist.removeItem('johndoe')
		const data = await persist.getItem('johndoe')
		expect(data).toEqual(undefined)
		done()
	})

})

describe('forEach', () => {

})

describe('clear', () => {

	test('clear list with single item', async done => {
		await persist.init()
		await persist.setItem('johndoe', {role: 'user'})
		await persist.clear()
		done()
	})

	test('clear empty list', async done => {
		await persist.init()
		await persist.clear()
		done()
	})

})