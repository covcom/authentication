
'use strict'

let data

module.exports.init = () => {
	data = []
	return
}

module.exports.setItem = async(key, val) => {
	const item = {key: key, val: val}
	data.push(item)
	const returnData = { file: '.node-persist/storage/hashvalue',
		content: {
			key: key,
			value: val,
			ttl: undefined
		} }
	return returnData
}

module.exports.getItem = async key => {
	for(const i in data) {
		if(data[i].key === key) {
			return data[i].val
		}
	}
	return undefined
}

module.exports.removeItem = async key => {
	for(const index in data) {
		if(data[index].key === key) {
			const item = {item: data[index].item, qty: data[index].qty}
			data.splice(index, 1)
			return item
		}
		return false
	}
}

module.exports.forEach = async callback => {
	console.log(data)
	for (const i in data) {
		await callback(data[i].qty)
	}
}

module.exports.clear = async() => data = []
