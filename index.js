#!/usr/bin/env node

'use strict'

const Koa = require('koa')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')

const app = new Koa()
app.use(bodyParser())
const router = new Router()

const status = require('http-status-codes')
const user = require('./modules/user')

const port = 8080

app.use( async(ctx, next) => {
	ctx.set('Access-Control-Allow-Origin', '*')
	ctx.set('content-type', 'application/json')
	await next()
})

router.head('/users/:user', async ctx => {
	try {
		const creds = ctx.get('Authorization')
		if(ctx.get('Authorization').length === 0) throw new Error('missing Authorization')
		const credentials = await user.extractCreds(creds)
		const valid = await user.checkAuth(credentials.username, credentials.password)
		if(valid === 'UNAUTHORIZED') throw new Error('invalid Authorization')
		ctx.status = status.OK
		ctx.body = {status: 'ok', message: user}
	} catch(err) {
		ctx.status = status.UNAUTHORIZED
		ctx.body = {status: 'error', message: err.message}
	}
})

router.post('/users', async ctx => {
	ctx.set('Allow', 'POST')
	try {
		if(ctx.get('Authorization').length === 0) {
			throw new Error('missing Authorization header')
		}
		const credentials = await user.extractCreds(ctx.get('Authorization'))
		const response = await user.add(credentials, ctx.request.body)
		ctx.status = status[response.replace(' ', '_')]
		ctx.body = {status: response}
	} catch(err) {
		ctx.status = status.BAD_REQUEST
		ctx.body = {status: 'error', message: err.message}
	}
})

app.use(router.routes())
app.use(router.allowedMethods())
const server = app.listen(port)

module.exports = server
