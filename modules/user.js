
'use strict'

const persist = require('node-persist')
const bcrypt = require('bcrypt')
const saltRounds = 10

module.exports.extractCreds = async header => {
	const [, hash] = header.split(' ')
	const plainText = Buffer.from(hash, 'base64').toString()
	const [user, pass] = plainText.split(':')
	return {username: user, password: pass}
}

module.exports.add = async(credentials, profile) => {
	await persist.init()
	const existing = await persist.getItem(credentials.username)
	console.log(existing)
	if(existing !== undefined) {
		console.log('undefined')
		return 'BAD REQUEST'
	}
	const hash = await bcrypt.hash(credentials.password, saltRounds)
	profile.password = hash
	await persist.setItem(credentials.username, profile)
	return 'CREATED'
}

module.exports.checkAuth = async(username, password) => {
	await persist.init()
	const existing = await persist.getItem(username)
	if(existing === undefined) return 'UNAUTHORIZED'
	const validPW = await bcrypt.compare(password, existing.password)
	if(!validPW) return 'UNAUTHORIZED'
	existing.username = username
	delete existing.password
	return existing
}

module.exports.clear = async() => {
	await persist.init()
	await persist.clear()
}
