#!/bin/sh

# paste clipboard to file
# pbpaste > <FILENAME>

git config user.name 'Mark Tyers'
git config user.email 'marktyers@gmail.com'
git config core.editor 'nano'
git config core.hooksPath .githooks
