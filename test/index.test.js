
'use strict'

const request = require('supertest')
const server = require('../index.js')
const user = require('../modules/user.js')
const status = require('http-status-codes')

const supertest = request(server)
// supertest.get(...);
// supertest.get(...);

beforeAll( async() => console.log('Jest starting!'))

beforeEach( async() => {
	await user.clear()
	const hash = Buffer.from('johndoe:P455w0rd').toString('base64')
	const header = `Basic ${hash}`
	await supertest.post('/users')
		.set('Accept', 'application/json')
		.set('Authorization', header)
		.send({role: 'user'})
		.expect(status.CREATED)
})

// close the server after each test
afterAll(() => {
	server.close()
	console.log('server closed!')
})

describe('HEAD /users/:user', () => {

	test('should return 401 if missing Authorization header', async done => {
		await supertest.head('/users/johndoe').expect(status.UNAUTHORIZED)
		done()
	})

	test('should return OK if valid Authorization header', async done => {
		await supertest.head('/users/johndoe')
			.auth('johndoe', 'P455w0rd')
			.expect(status.OK)
		done()
	})

	test('should return 401 if the username is invalid', async done => {
		await supertest.head('/users/johndoe')
			.auth('janedoe', 'P455w0rd')
			.expect(status.UNAUTHORIZED)
		done()
	})

})

describe('POST /users', () => {
	test('adding a valid user', async done => {
		const hash = Buffer.from('jdoe:password').toString('base64')
		const header = `Basic ${hash}`
		await supertest.post('/users')
			.set('Accept', 'application/json')
			.set('Authorization', header)
			.send({role: 'user'})
			.expect(status.CREATED)
			.expect( res => {
				res.body.account = 'johndoe'
				res.body.type = 'user'
	  			done()
			})
	})

	test('return UNAUTHORIZED if Authorization header missing', async done => {
		await supertest.post('/users').expect(status.BAD_REQUEST)
		done()
	})

})
