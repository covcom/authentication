
'use strict'

const bcrypt = require('bcrypt')
const base64 = require('base-64')
const user = require('../modules/user.js')

describe('extractCreds', () => {

	test('correctly decodes valid auth string', async done => {
		const hash = base64.encode('username:password')
		const header = `Basic ${hash}`
		const data = await user.extractCreds(header)
		expect(data).toEqual({username: 'username', password: 'password'})
		done()
	})

})

describe('add', () => {

	test('adding a new user', async done => {
		await user.clear()
		const creds = {username: 'username', password: 'password'}
		const profile = {type: 'user'}
		const data = await user.add(creds, profile)
		expect(data).toEqual('CREATED')
		done()
	})

	test('adding an existing user should throw error', async done => {
		await user.clear()
		const creds = {username: 'username', password: 'password'}
		const profile = {type: 'user'}
		await user.add(creds, profile)
		console.log('adding same user for the second time')
		const data = await user.add(creds, profile)
		expect(data).toEqual('BAD REQUEST')
		done()
	})

})

describe('checkAuth', () => {

	test('valid username and password returns profile', async done => {
		await user.clear()
		await user.add({username: 'username', password: 'password'}, {type: 'user'})
		const data = await user.checkAuth('username', 'password')
		expect(data).toEqual({username: 'username', type: 'user'})
		done()
	})

	test('check for invalid username', async done => {
		await user.clear()
		await user.add({username: 'johndoe', password: 'pword'}, {type: 'user'})
		const data = await user.checkAuth('janedoe', 'pword')
		expect(data).toEqual('UNAUTHORIZED')
		done()
	})

	test('check for invalid password', async done => {
		await user.clear()
		await user.add({username: 'johndoe', password: 'pword'}, {type: 'user'})
		const data = await user.checkAuth('johndoe', 'badpassword')
		expect(data).toEqual('UNAUTHORIZED')
		done()
	})
})
